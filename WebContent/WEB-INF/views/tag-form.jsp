<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Tag form</title>
</head>
<body>
<form:form method="post" modelAttribute="tag">
	<form:label path="name">Name:</form:label>
	<form:input path="name" />
	<input type="submit" value="Submit" />
</form:form>
</body>
</html>