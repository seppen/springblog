<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1 align="center">Comment form, article = ${comment.articleID}</h1>
	<form:form method="post" modelAttribute="comment">
		<table bgcolor="#EEEEEE" align="center">
			<tr>
				<td><form:label path="author">Author:</form:label></td>
				<td><form:input path="author" /></td>
			</tr>
			<tr>
				<td><form:label path="text">Text:</form:label></td>
				<td><form:textarea path="text" /></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="Submit" /></td>
			</tr>
		</table>
	</form:form>
</body>
</html>