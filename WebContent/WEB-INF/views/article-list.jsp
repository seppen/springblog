<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="<c:url value = "/resources/style.css"/>"
	type="text/css">
<title>SpringBlog</title>
</head>
<body>
	<div id="container">
		<div id="header">
			<h1>SpringBlog</h1>
		</div>
		<div id="navigation">
			<ul>
				<li><a href="#">Home</a></li>
				<li><a href="<c:url value="/article/add"/>">Add Article</a></li>
			</ul>
		</div>
		<div id="content-container">
			<div id="content">
				<h2>Articles</h2>
				<c:forEach items="${articles}" var="article">
					<div class="article">
						<h3 class="title">
							<a href="<c:url value="/article/${article.id}"/>">
								<c:out value="${article.title}" />
							</a>
						</h3>
						<p class="author">
							author: <c:out value="${article.author}" />
						</p>
						<hr>
						<p class="text">
							<c:out value="${article.text}" />
						</p>
						<hr>
						<p class="left">dateCreated: ${article.dateCreated}</p>
						<p class="right">
							<a href="<c:url value="/article/${article.id}"/>">
								View detailed
							</a>
						</p>
					</div>
				</c:forEach>
			</div>
			<div id="aside">
				<c:import url="/archive" />
			</div>
			<div id="footer">Copyright � Site name, 20XX</div>
		</div>
	</div>
</body>
</html>