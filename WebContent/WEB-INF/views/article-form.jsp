<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Article</title>
</head>
<body>
	<h1 align="center">Article form</h1>
	<form:form method="post" modelAttribute="formData">
		<table bgcolor="#EEEEEE" align="center">
			<tr>
				<td><form:label path="article.title">Title:</form:label></td>
				<td><form:input path="article.title" /></td>
				<td></td>
			</tr>
			<tr>
				<td><form:label path="article.author">Author:</form:label></td>
				<td><form:input path="article.author" /></td>
				<td>Select from these tags (<a href=<c:url value="/tag/add"/>>add more</a>)</td>
			</tr>
			<tr>
				<td><form:label path="article.text">Text:</form:label></td>
				<td><form:textarea path="article.text" rows="18" cols="40" /></td>
				<td><form:select path="articleTagIDs" multiple="true" size="18">
						<form:options items="${tags}" itemValue="id" itemLabel="name" />
					</form:select></td>
			</tr>
			<tr>
				<td colspan="3"><input type="submit" value="Submit" /></td>
			</tr>
		</table>
	</form:form>
</body>
</html>