<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="<c:url value = "/resources/style.css"/>"
	type="text/css">
<title>Article</title>
</head>
<body>
	<div id="container">
		<div id="header">
			<h1>SpringBlog</h1>
		</div>
		<div id="navigation">
			<ul>
				<li><a href="<c:url value="/" />">Home</a></li>
				<li><a href="<c:url value="/article/add"/>">Add Article</a></li>
			</ul>
		</div>
		<div id="content-container">
			<div id="content">
				<div class="article">
					<h3 class="title">
						<c:out value="${article.title}" />
					</h3>
					<p class="author">
						author:
						<c:out value="${article.author}" />
					</p>
					<hr>
					<p class="text">
						<c:out value="${article.text}" />
					</p>
					<hr>
					<p class="left">dateCreated: ${article.dateCreated}</p>
					<p class="right">
						tags: 
						<c:forEach items="${articleTags}" var="tag">
							<c:out value="${tag.name}" /> &nbsp;
						</c:forEach>
					</p>
				</div>
				
				<a href=<c:url value="${article.id}/edit" />>Edit article</a>
				<a href=<c:url value="${article.id}/delete" />>Delete article</a>

				<p>Comments:</p>

				<table class="articlecomments">
					<tr>
						<td>author</td>
						<td>dateCreated</td>
						<td>text</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<c:forEach items="${comments}" var="comment">
						<tr>
							<td><c:out value="${comment.author}" /></td>
							<td>${comment.dateCreated}</td>
							<td><c:out value="${comment.text}" /></td>
							<td><a href=<c:url value="/comment/${comment.id}/edit" />>edit</a></td>
							<td><a href=<c:url value="/comment/${comment.id}/delete" />>delete</a></td>
						</tr>
					</c:forEach>
				</table>

				<p>
					<a href=<c:url value="/comment/add/${articleID}"/>>Add comment</a>
				</p>
			</div>

			<div id="aside">
				<c:import url="/archive" />
			</div>
		</div>
	</div>
</body>
</html>