<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<h3>Archive</h3>
<c:set var="year" value="" />
<c:set var="month" value="" />
<c:forEach items="${archive}" var="entry">
	<c:if test="${year != entry.year}">
		<p>${entry.year}</p>
	</c:if>
	<c:set var="year" value="${entry.year}" />
	<c:if test="${month != entry.month}">
		<p>${entry.month}</p>
	</c:if>
	<c:set var="month" value="${entry.month}" />
	<a href=<c:url value="/article/${entry.id}"/>>${entry.title}</a>
	<br>
</c:forEach>