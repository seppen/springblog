package springblog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import springblog.manager.CommentManager;
import springblog.pojo.Comment;

@Controller
public class CommentController {
	
	@Autowired
	private CommentManager commentManager = null;
	
	
	@RequestMapping(value = {"/comment/{commentID}/delete"})
	public String deleteComment(@PathVariable int commentID, Model model) {
		Comment comment = commentManager.getCommentByID(commentID);
		if (null == comment) {
			model.addAttribute("message", "No such comment.");
			return "message";
		}
		commentManager.delete(comment);
		return "redirect:/article/" + comment.getArticleID();
	}
}
