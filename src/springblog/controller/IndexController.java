package springblog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import springblog.manager.ArticleManager;
import springblog.pojo.ArchivedArticle;

@Controller
@RequestMapping(value = {"", "/"})
public class IndexController {
	
	@Autowired
	private ArticleManager articleManager;
	
	
	@RequestMapping
	public String indexAction() {
		return "redirect:/article";
	}
	
	@RequestMapping(value = {"/archive"})
	public String viewArchive(Model model) {
		model.addAttribute("archive",
				ArchivedArticle.getArticleArchive(articleManager.getAllArticles()));
		return "archive";
	}
}
