package springblog.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import springblog.manager.ArticleManager;
import springblog.manager.TagManager;
import springblog.pojo.Article;
import springblog.pojo.Tag;

@Controller
public class ArticleFormController {

	@Autowired
	private ArticleManager articleManager;
	
	@Autowired
	private TagManager tagManager;
	
	
	@RequestMapping(value = {"/article/add"}, method = RequestMethod.GET)
	public String addArticle(Model model) {
		List<Tag> allPresentTags = tagManager.getAllTags();
		model.addAttribute("formData", new ArticleFormData());
		model.addAttribute("tags", allPresentTags);
		return "article-form";
	}
	
	
	@RequestMapping(value = {"/article/add"}, method = RequestMethod.POST)
	public String addArticle(ArticleFormData formData, Model model) {
		Article article = formData.getArticle();
		articleManager.insert(article);
		articleManager.setArticleTags(article, formData.getArticleTagIDs());
		return "redirect:/article/" + article.getId();
	}
	
	
	@RequestMapping(value = {"/article/{articleID}/edit"}, method = RequestMethod.GET)
	public String editArticle(@PathVariable int articleID, Model model) {
		Article article = articleManager.getArticleByID(articleID);
		if (null == article) {
			model.addAttribute("message", "No such article.");
			return "message";
		}
		ArticleFormData formData = new ArticleFormData();
		formData.setArticle(article);
		model.addAttribute("formData", formData);
		model.addAttribute("tags", tagManager.getAllTags());
		return "article-form";
	}
	
	
	@RequestMapping(value = {"/article/{articleID}/edit"}, method = RequestMethod.POST)
	public String editArticle(@PathVariable int articleID, ArticleFormData formData, Model model) {
		Article article = formData.getArticle();
		article.setId(articleID);
		articleManager.update(article);
		articleManager.setArticleTags(article, formData.getArticleTagIDs());
		return "redirect:/article/" + article.getId();
	}
}


class ArticleFormData {
	
	private List<Integer> articleTagIDs;
	private Article article;
	
	public ArticleFormData() {
		article = new Article();
		articleTagIDs = new ArrayList<Integer>();
	}
	
	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public List<Integer> getArticleTagIDs() {
		return articleTagIDs;
	}

	public void setArticleTagIDs(List<Integer> articleTagIDs) {
		this.articleTagIDs = articleTagIDs;
	}
}
