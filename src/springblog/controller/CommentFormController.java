package springblog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import springblog.manager.ArticleManager;
import springblog.manager.CommentManager;
import springblog.pojo.Comment;

@Controller
public class CommentFormController {
	
	@Autowired
	private ArticleManager articleManager = null;
	
	@Autowired
	private CommentManager commentManager = null;
	
	
	@RequestMapping(value = {"/comment/add/{commentArticleID}"}, method = RequestMethod.GET)
	public String addComment(@PathVariable int commentArticleID, Model model) {
		if (null == articleManager.getArticleByID(commentArticleID)) {
			model.addAttribute("message", "Cannot add comment for a non-existing article.");
			return "message";
		}
		Comment comment = new Comment();
		comment.setArticleID(commentArticleID);
		model.addAttribute("comment", comment);
		return "comment-form";
	}
	
	
	@RequestMapping(value = {"/comment/add/{commentArticleID}"}, method = RequestMethod.POST)
	public String addComment(@PathVariable int commentArticleID, Comment comment, Model model) {
		comment.setArticleID(commentArticleID);
		commentManager.insert(comment);
		return "redirect:/article/" + comment.getArticleID();
	}
	
	
	@RequestMapping(value = {"/comment/{commentID}/edit"}, method = RequestMethod.GET)
	public String editComment(@PathVariable int commentID, Model model) {
		Comment comment = commentManager.getCommentByID(commentID);
		if (null == comment) {
			model.addAttribute("message", "No such comment.");
			return "message";
		}
		model.addAttribute("comment", comment);
		return "comment-form";
	}
	
	
	@RequestMapping(value = {"/comment/{commentID}/edit"}, method = RequestMethod.POST)
	public String editComment(@PathVariable int commentID, Comment comment, Model model) {
		comment.setId(commentID);
		commentManager.update(comment);
		//quick fix for the redirect below
		comment = commentManager.getCommentByID(commentID);
		return "redirect:/article/" + comment.getArticleID();
	}
}
