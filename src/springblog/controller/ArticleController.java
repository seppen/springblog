package springblog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import springblog.manager.ArticleManager;
import springblog.manager.CommentManager;
import springblog.pojo.Article;
import springblog.pojo.ArchivedArticle;

@Controller
@RequestMapping(value = {"/article"})
public class ArticleController {
	
	@Autowired
	private ArticleManager articleManager = null;
	
	@Autowired
	private CommentManager commentManager = null;
	
	
	@RequestMapping
	public String viewArticleList(Model model) {
		List<Article> articles = articleManager.getAllArticles();
		model.addAttribute("articles", articles);
		return "article-list";
	}
	
	
	@RequestMapping(value = {"/{articleID}"})
	public String viewArticle(@PathVariable int articleID, Model model) {
		Article article = articleManager.getArticleByID(articleID);
		if (null == article) {
			model.addAttribute("message", "No such article.");
			return "message";
		}
		model.addAttribute("article", article);
		model.addAttribute("comments", commentManager.getCommentsByArticle(article));
		model.addAttribute("archive",
				ArchivedArticle.getArticleArchive(articleManager.getAllArticles()));
		model.addAttribute("articleTags", articleManager.getArticleTags(article));
		return "article-view";
	}
	
	
	@RequestMapping(value = {"/{articleID}/delete"})
	public String deleteArticle(@PathVariable int articleID, Model model) {
		Article article = articleManager.getArticleByID(articleID);
		if (null == article) {
			model.addAttribute("message", "No such article.");
			return "message";
		}
		articleManager.delete(article);
		return "redirect:/";
	}
}