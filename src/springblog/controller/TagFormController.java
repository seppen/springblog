package springblog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import springblog.manager.TagManager;
import springblog.pojo.Tag;

@Controller
@RequestMapping(value = {"/tag"})
public class TagFormController {
	
	@Autowired
	private TagManager tagManager = null;
	
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addTag(Model model) {
		model.addAttribute("tag", new Tag());
		return "tag-form";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addTag(Tag tag, Model model) {
		tagManager.insert(tag);
		return "redirect:/tag/add";
	}
}
