package springblog.pojo;

public class Comment {
	
	private int id = 0;
	private int articleID = 0;
	private String text = "";
	private String author = "";
	private String dateCreated = "";
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getArticleID() {
		return articleID;
	}
	
	public void setArticleID(int articleID) {
		this.articleID = articleID;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public String getAuthor() {
		return author;
	}
	
	public void setAuthor(String author) {
		this.author = author;
	}
	
	public String getDateCreated() {
		return dateCreated;
	}
	
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
}
