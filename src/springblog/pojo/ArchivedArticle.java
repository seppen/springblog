package springblog.pojo;

import java.util.LinkedList;
import java.util.List;


//wtf???
public class ArchivedArticle {
	
	private String month = "";
	private String title = "";
	private int id = 0;
	private String year = "";
	
	public static List<ArchivedArticle> getArticleArchive(List<Article> articles) {
		List<ArchivedArticle> archive = new LinkedList<ArchivedArticle>();
		for (Article article : articles) {
			ArchivedArticle entry = new ArchivedArticle();
			entry.setYear(article.getDateCreated().substring(0, 4));
			entry.setTitle(article.getTitle());
			entry.setId(article.getId());
			int monthID = Integer.parseInt(article.getDateCreated().substring(5, 7));
			entry.setMonth(getMonthName(monthID));
			archive.add(entry);
		}
		return archive;
	}
	
	private static String getMonthName(int month) {
		switch (month) {
			case 1: return "January";
			case 2: return "February";
			case 3: return "March";
			case 4: return "April";
			case 5: return "May";
			case 6: return "June";
			case 7: return "July";
			case 8: return "August";
			case 9: return "September";
			case 10: return "October";
			case 11: return "November";
			case 12: return "December";
		}
		return "unknown";
	}
	
	public String getMonth() {
		return month;
	}
	
	public void setMonth(String month) {
		this.month = month;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getYear() {
		return year;
	}
	
	public void setYear(String year) {
		this.year = year;
	}
}