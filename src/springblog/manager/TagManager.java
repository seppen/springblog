package springblog.manager;

import java.util.List;

import springblog.pojo.Tag;

public interface TagManager {
	
	public void insert(Tag tag);
	
	public List<Tag> getAllTags();
	
}
