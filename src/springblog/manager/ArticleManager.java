package springblog.manager;

import java.util.List;

import springblog.pojo.Article;
import springblog.pojo.Tag;

public interface ArticleManager {
	
	public void insert(Article article);
	
	public void update(Article article);
	
	public void delete(Article article);
	
	public Article getArticleByID(int articleID);
	
	public List<Article> getAllArticles();
	
	public void setArticleTags(Article article, List<Integer> tagIDs);
	
	public List<Tag> getArticleTags(Article article);
	
}
