package springblog.manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import springblog.pojo.Tag;

public class TagManagerJdbcImpl implements TagManager {
	
	private JdbcTemplate jdbcTemplate;
	
	
	public TagManagerJdbcImpl(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public void insert(Tag tag) {
		String sql = "insert into Tag (name) value (?) ";
		jdbcTemplate.update(sql, tag.getName());
		int id = jdbcTemplate.queryForInt("select max(id) from Tag ");
		tag.setId(id);
	}
	
	@Override
	public List<Tag> getAllTags() {
		String sql = "select * from Tag ";
		List<Tag> tags = jdbcTemplate.query(sql, new TagRowMapper());
		return tags;
	}
}


class TagRowMapper implements RowMapper<Tag> {

	@Override
	public Tag mapRow(ResultSet rs, int numRow) throws SQLException {
		Tag tag = new Tag();
		tag.setId(rs.getInt("id"));
		tag.setName(rs.getString("name"));
		return tag;
	}
	
}
