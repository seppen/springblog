package springblog.manager;

import java.util.List;

import springblog.pojo.Article;
import springblog.pojo.Comment;

public interface CommentManager {
	
	public void insert(Comment comment);
	
	public void update(Comment comment);
	
	public void delete(Comment comment);
	
	public Comment getCommentByID(int commentID);
	
	public List<Comment> getCommentsByArticle(Article article);
	
}
